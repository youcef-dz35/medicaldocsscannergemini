<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NewGeminiController;
use App\Http\Controllers\GeminiLabResultsController;




Route::post('LabTestGemini', [GeminiLabResultsController::class, 'LabTestGemini']);
Route::post('NewLabTestGemini', [NewGeminiController::class, 'GeminiCategorization']);
