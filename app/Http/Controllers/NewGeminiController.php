<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use GeminiAPI\Laravel\Facades\Gemini;

class NewGeminiController extends BaseController
{
    //


    public function GeminiCategorization(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,jpg,png',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        return Gemini::generateText('PHP in less than 100 chars');
    }
}
