<?php

namespace App\Http\Controllers;

use Gemini;
use Validator;
use Gemini\Data\Blob;
use Gemini\Enums\MimeType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\BaseController;
use json_last_error_pos;
class GeminiLabResultsController extends BaseController
{
    //

    public function LabTestGemini(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,jpg,png',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }



        // Get uploaded image data
        // $imageData = $request->file('image')->get();

        // Convert image to base64 string
        // $base64Image = base64_encode($imageData);

        // Build prompt with base64 image data
        // Build prompt with base64 image data and your specified instructions
        $newPrompt = "extract the patient's first name, last name, age, and the date from the document and return the data as a JavaScript object called results, inside of it should be 2 attributes one called category which will be a string mentioning the category and second should be an array called cells, in that array there should be objects with the attributes: title, value, normalRange, minRange, maxRange and unit.
                        The object should look like this:
                        {{
                        \"patient\": {{
                            \"firstName\": \"Extracted first name\",
                            \"lastName\": \"Extracted last name\",
                            \"age\": \"Extracted age\"
                        }},
                        \"date\": \"Extracted date\",
                        \"cells\": [
                            {{
                                \"title\": \"Glucose\",
                                \"value\": \"9\",
                                \"normalRange\": \"8 - 11\",
                                \"minRange\": \"8\",
                                \"maxRange\": \"11\",
                                \"unit\": \"mg\"
                            }}
                        ]
                    }}";


        // Access Gemini API
        // $metadataPrompt = "extractthe from medical doc first name, last name, age, category and the date of production from the document and return the data as a JavaScript object";
        $geminiAPIKey = getenv('Gemini_API_Key');
        $client = Gemini::client($geminiAPIKey);

        // return $client->models()->list()->models;
        // $result = $client->geminiPro()->generateContent($prompt);
        $result = $client
                    ->geminiProVision()
                    ->generateContent([
                        $newPrompt,
                        new Blob(
                            mimeType: MimeType::IMAGE_JPEG,
                            data: base64_encode(
                                file_get_contents($request->file('image'))
                            )
                        )
                        ]
                    );

        // dd($result);
                    // Parse Gemini response and structure data
        // verifying  that we got a valid JSON response from Gemini
        $newclient = Gemini::client($geminiAPIKey);
        // $unpureJson = $newclient->geminiPro()->generateContent('Use this string response to generate a valid JSON format so It will be easy to switch from string to json using PHP json_decode ('.$result->text().')');
        $purificationPrompt = "Please provide a string containing JSON data. Ensure that the JSON data is enclosed within curly braces ({}) and is not embedded within other code or text. Example Input String: {\"patient\": {\"firstName\": \"Yash\",\"lastName\": \"Patel\",\"age\": \"21 Years\",},\"date\": \"02/12/2023\",\"cells\": [{\"title\": \"Glucose\",\"value\": \"9\",\"normalRange\": \"8 - 11\",\"minRange\": \"8\",\"maxRange\": \"11\",\"unit\": \"mg\",},{\"title\": \"Urea\",\"value\": \"15\",\"normalRange\": \"13 - 43\",\"minRange\": \"13\",\"maxRange\": \"43\",\"unit\": \"mg/dL\",}],}Once you provide the input string, I will process it and output a string that can be switched to JSON format using json_decode. This is the String I need to purify:";
        $unpureJson = $newclient->geminiPro()->generateContent($purificationPrompt.$result->text());
        // return $unpureJson;
        // $string = str_replace('```json', '', $unpureJson->text());
        // $string = str_replace('```JSON', '', $unpureJson->text());
        // $string = str_replace('```', '', $string);
        // $string = preg_replace('/^php\n\$jsonString\s+```js\s*const\s+results\s*=\s*/', '', $string);
        // Find the index of the first '{' character
        $start_index = strpos($unpureJson->text(), '{');

        // Extract the substring from the start_index onwards
        $json_string = substr($unpureJson->text(), $start_index);

        // Decode JSON string
        $json_string = str_replace('```', '', $json_string);
        $labResults = json_decode($json_string);
        // return [$json_string,$labResults];
        // Check if JSON decoding was successful
        if ($labResults === null && json_last_error() !== JSON_ERROR_NONE) {
            return $this->sendError('Error decoding JSON.', json_last_error_msg());
        } else {
            // Print formatted JSON

            return $this->sendResponse($labResults, 'Lab results extracted successfully');
        }


        // return $this->sendResponse($responseData, 'Lab results extracted successfully');

    }
    private function parseResponseData($response)
    {
        // Implement logic to parse the Gemini response (JSON format)
        // and extract the desired data following the structure specified in your prompt.
        // This involves parsing relevant sections based on your prompt instructions
        // and building the necessary JSON object with patient information, date, and cells array.

        // Example (replace with your actual parsing logic):
        return [
            "patient" => [
                "firstName" => "Extracted first name (from Gemini response)",
                "lastName" => "Extracted last name (from Gemini response)",
                "age" => "Extracted age (from Gemini response)",
            ],
            "date" => "Extracted date (from Gemini response)",
            "cells" => [
                [
                    "title" => "Glucose",
                    "value" => "9",
                    "normalRange" => "8 - 11",
                    "minRange" => "8",
                    "maxRange" => "11",
                    "unit" => "mg",
                ],
                // ... Add more objects for additional test results (parsed from Gemini response)
            ],
        ];
    }


    public function DLabTestGemini(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,jpg,png',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        // Access Gemini API
        $geminiAPIKey = getenv('Gemini_API_Key');
        $client = Gemini::client($geminiAPIKey);

        // Build prompt with base64 image data
        $prompt = "Extract the patient's first name, last name, age, and the date from the document. Return the data as a JavaScript object with the following structure:
        {
            \"patient\": {
                \"firstName\": \"Extracted first name\",
                \"lastName\": \"Extracted last name\",
                \"age\": \"Extracted age\"
            },
            \"date\": \"Extracted date\",
            \"cells\": [
                {
                    \"title\": \"Glucose\",
                    \"value\": \"9\",
                    \"normalRange\": \"8 - 11\",
                    \"minRange\": \"8\",
                    \"maxRange\": \"11\",
                    \"unit\": \"mg\"
                },
                // ... other cells
            ]
        }";

        // Generate content using Gemini Pro Vision
        $response = $client->geminiProVision()->generateContent([
            $prompt,
            new Blob(
                mimeType: MimeType::IMAGE_JPEG,
                data: base64_encode(
                    file_get_contents($request->file('image'))
                )
            )
        ]);


        // Parse JSON response
        $labResults = json_decode($response->text(), true);

        // Check for parsing errors
        if (json_last_error() !== JSON_ERROR_NONE) {
            // Log the error for debugging
            \Log::error('Error decoding JSON: ' . json_last_error_msg());

            // Try to find the error location
            $errorPosition = json_last_error_pos();
            $errorContext = substr($response->text(), $errorPosition - 20, 40);

            return $this->sendError('Error decoding JSON.', 'Syntax error at position ' . $errorPosition . ': ' . $errorContext);
        }

        // Validate extracted data
        if (!isset($labResults['patient']) ||
            !isset($labResults['date']) ||
            !isset($labResults['cells']) ||
            !is_array($labResults['cells'])) {
            return $this->sendError('Invalid response format.', 'Missing required fields.');
        }

        foreach ($labResults['cells'] as $cell) {
            if (!isset($cell['title']) ||
                !isset($cell['value']) ||
                !isset($cell['normalRange']) ||
                !isset($cell['minRange']) ||
                !isset($cell['maxRange']) ||
                !isset($cell['unit'])) {
                return $this->sendError('Invalid response format.', 'Missing cell data.');
            }
        }

        // Return the parsed lab results
        return $this->sendResponse($labResults, 'Lab results extracted successfully');
    }
}
